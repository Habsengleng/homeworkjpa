package com.example.amsproject.controller;

import com.example.amsproject.repository.model.BaseApiResponse;
import com.example.amsproject.repository.model.Category;
import com.example.amsproject.repository.model.CatrgoryUpdate;
import com.example.amsproject.service.CategorySerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/category")
public class CategoryRestController {
    CategorySerivce categorySerivce;
    @Autowired
    public void setCategorySerivce(CategorySerivce categorySerivce){
        this.categorySerivce=categorySerivce;
    }

    @PostMapping
    ResponseEntity<BaseApiResponse<Category>> insertCategory(@RequestBody Category category){
        BaseApiResponse<Category> categoryBaseApiResponse = new BaseApiResponse<>();
        categoryBaseApiResponse.setData(categorySerivce.insertCategory(category));
        categoryBaseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        categoryBaseApiResponse.setMessage("Category has been inserted");
        categoryBaseApiResponse.setStatus(HttpStatus.OK);
        return new ResponseEntity<BaseApiResponse<Category>>(categoryBaseApiResponse, HttpStatus.CREATED);
    }
    @GetMapping
    ResponseEntity<BaseApiResponse<List<Category>>> getAll(){
        List<Category> categoryList = categorySerivce.getAll();
        BaseApiResponse<List<Category>> categoryBaseApiResponse = new BaseApiResponse<>();
        categoryBaseApiResponse.setData(categoryList);
        categoryBaseApiResponse.setStatus(HttpStatus.OK);
        categoryBaseApiResponse.setMessage("Categories have been selected");
        categoryBaseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(categoryBaseApiResponse,HttpStatus.OK);
    }
    @PutMapping("{id}")
    ResponseEntity<BaseApiResponse<CatrgoryUpdate>> update(@RequestBody CatrgoryUpdate category, @PathVariable int id){
        BaseApiResponse<CatrgoryUpdate> categoryBaseApiResponse = new BaseApiResponse<>();
        CatrgoryUpdate catrgoryUpdate = categorySerivce.update(category,id);
        categoryBaseApiResponse.setData(catrgoryUpdate);
        categoryBaseApiResponse.setMessage("Category has been updated");
        categoryBaseApiResponse.setStatus(HttpStatus.OK);
        categoryBaseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<>(categoryBaseApiResponse,HttpStatus.OK);
    }
    @DeleteMapping("{id}")
    ResponseEntity<BaseApiResponse<Category>> delete(@PathVariable int id){
        BaseApiResponse<Category> categoryBaseApiResponse = new BaseApiResponse<>();
        Category category = categorySerivce.delete(id);
        categoryBaseApiResponse.setData(category);
        categoryBaseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        categoryBaseApiResponse.setStatus(HttpStatus.OK);
        categoryBaseApiResponse.setMessage("Category has been selected");
        return new ResponseEntity<>(categoryBaseApiResponse,HttpStatus.OK);
    }
}
