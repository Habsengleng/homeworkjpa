package com.example.amsproject.controller;

import com.example.amsproject.repository.message.MessageProperties;
import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.ArticleUpdate;
import com.example.amsproject.repository.model.BaseApiResponse;
import com.example.amsproject.repository.model.Category;
import com.example.amsproject.service.ArticleService;
import com.example.amsproject.service.CategorySerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/article")
public class AritcleRestController {
    ArticleService articleService;
    public MessageProperties messageProperties;
    @Autowired
    public void setArticleService(ArticleService articleService){
        this.articleService=articleService;
    }
    @Autowired
    public void setMessageProperties(MessageProperties messageProperties) {
        this.messageProperties = messageProperties;
    }

    @PostMapping
    ResponseEntity<BaseApiResponse<Article>> save(@RequestBody Article article){
        BaseApiResponse<Article> response = new BaseApiResponse<>();
        Article article1 = articleService.save(article);
        response.setData(article1);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Article has been inserted");
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<BaseApiResponse<Article>>(response, HttpStatus.CREATED);
    }
    @GetMapping
    ResponseEntity<BaseApiResponse<List<Article>>> getAll(){
        BaseApiResponse<List<Article>> response = new BaseApiResponse<>();
        List<Article> articles = articleService.getAll();
        HttpStatus httpStatus=HttpStatus.OK;
        if(articles==null){
            response.setData(null);
            response.setMessage("Selected fail");
            response.setStatus(HttpStatus.BAD_REQUEST);
            httpStatus = HttpStatus.BAD_GATEWAY;
        }else{
            response.setData(articles);
            response.setStatus(HttpStatus.OK);
            response.setMessage("Articles have been selected");
        }
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<BaseApiResponse<List<Article>>>(response,httpStatus);
    }
    @GetMapping("{id}")
    ResponseEntity<BaseApiResponse<Article>> getOne(@PathVariable int id){
        BaseApiResponse<Article> response = new BaseApiResponse<>();
        Article article = articleService.getOne(id);
        HttpStatus httpStatus=HttpStatus.OK;
        if(article==null){
            response.setData(null);
            response.setMessage("Selected fail");
            response.setStatus(HttpStatus.BAD_REQUEST);
            httpStatus = HttpStatus.BAD_GATEWAY;
        }else {
            response.setData(article);
            response.setStatus(HttpStatus.OK);
            response.setMessage("Article has been selected");
        }
            response.setTime(new Timestamp(System.currentTimeMillis()));
            return new ResponseEntity<BaseApiResponse<Article>>(response, HttpStatus.OK);

    }
    @GetMapping("/category")
    ResponseEntity<BaseApiResponse<List<Article>>> getByCategoryTitle(@RequestParam String title){
        BaseApiResponse<List<Article>> response = new BaseApiResponse<>();
        List<Article> article2 = articleService.getByCategoryTitle(title);
        response.setData(article2);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Articles have been selected");
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return new ResponseEntity<BaseApiResponse<List<Article>>>(response,HttpStatus.OK);
    }
    @PutMapping("{id}")
    ResponseEntity<BaseApiResponse<ArticleUpdate>> update(@RequestBody ArticleUpdate article, @PathVariable int id){
        ArticleUpdate articleUpdate = articleService.update(article,id);
        BaseApiResponse<ArticleUpdate> response = new BaseApiResponse<>();
        HttpStatus httpStatus=HttpStatus.OK;
        if(articleUpdate==null){
            response.setData(null);
            httpStatus=HttpStatus.BAD_GATEWAY;
            response.setStatus(HttpStatus.BAD_GATEWAY);
            response.setMessage("Updated fail");
        }else {
            response.setData(articleUpdate);
            response.setStatus(HttpStatus.OK);
            response.setMessage("Articles have been updated");
        }
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<BaseApiResponse<ArticleUpdate>>(response,httpStatus);
    }
    @DeleteMapping("/{id}")
    ResponseEntity<BaseApiResponse<Article>> deleteCategory(@PathVariable int id){
        Article deleted = articleService.delete(id);
        BaseApiResponse<Article> response = new BaseApiResponse<>();
        response.setData(deleted);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Articles have been deleted");
        response.setTime(new Timestamp(System.currentTimeMillis()));
       return new ResponseEntity<BaseApiResponse<Article>>(response,HttpStatus.OK);
    }
}
