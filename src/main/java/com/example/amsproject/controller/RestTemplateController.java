package com.example.amsproject.controller;

import com.example.amsproject.repository.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("restTemplate")
public class RestTemplateController {
    @Autowired
    RestTemplate restTemplate;

    final String ROOT_URI_ARTICLE = "http://localhost:8080/article";
    final String ROOT_URI_CATEGORY = "http://localhost:8080/category";
    final String GET_BY_CATEGORY_API = "http://localhost:8080/article/category";

    @PostMapping("/article")
    public ResponseEntity<BaseApiResponse> create(@RequestBody Article article) {
        ResponseEntity<BaseApiResponse> response = restTemplate.postForEntity(ROOT_URI_ARTICLE,article,BaseApiResponse.class);
        return response;
    }
    @GetMapping("/article")
    public ResponseEntity<BaseApiResponse> getAll() {
        return restTemplate.getForEntity(ROOT_URI_ARTICLE, BaseApiResponse.class);
    }
    @GetMapping("/article/{id}")
    public ResponseEntity<BaseApiResponse> getById(@PathVariable int id) {
      return restTemplate.getForEntity(ROOT_URI_ARTICLE + "/"+id, BaseApiResponse.class);
    }
    @PutMapping("/article/{id}")
    public ResponseEntity<BaseApiResponse> update(@PathVariable("id") int id, @RequestBody ArticleUpdate article) {
        HttpEntity<ArticleUpdate> request = new HttpEntity<>(article);
        request.getBody().setCategory(article.getCategory());
        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);
        ResponseEntity<BaseApiResponse> responseEntity =
                restTemplate.exchange(ROOT_URI_ARTICLE +"/"+id,HttpMethod.PUT,request,BaseApiResponse.class,params);
        return responseEntity;
    }
    @GetMapping("/article/category")
    public ResponseEntity<BaseApiResponse> getCategoryByTitle(@RequestParam String title){
        ResponseEntity<BaseApiResponse> response = restTemplate.getForEntity(GET_BY_CATEGORY_API + "?title="+title,BaseApiResponse.class);
        return response;
    }
    @DeleteMapping("/article/{id}")
    public ResponseEntity<BaseApiResponse> deleteArticle(@PathVariable int id){
        ResponseEntity<BaseApiResponse> response = restTemplate.exchange(ROOT_URI_ARTICLE +"/"+id, HttpMethod.DELETE,null,BaseApiResponse.class);
        return response;
    }
    //TODO: Rest template for Category

    @GetMapping("/category")
    public ResponseEntity<BaseApiResponse> getAllCategory() {
        return restTemplate.getForEntity(ROOT_URI_CATEGORY, BaseApiResponse.class);
    }
    @PostMapping("/category")
    public ResponseEntity<BaseApiResponse> saveCategory(@RequestBody Category category) {
        ResponseEntity<BaseApiResponse> response = restTemplate.postForEntity(ROOT_URI_CATEGORY,category,BaseApiResponse.class);
        return response;
    }
    @DeleteMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse> deleteCategory(@PathVariable int id){
        ResponseEntity<BaseApiResponse> response = restTemplate.exchange(ROOT_URI_CATEGORY +"/"+id, HttpMethod.DELETE,null,BaseApiResponse.class);
        return response;
    }
    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse> updateCategory(@PathVariable("id") int id, @RequestBody CatrgoryUpdate catrgoryUpdate) {
        HttpEntity<CatrgoryUpdate> request = new HttpEntity<>(catrgoryUpdate);
        request.getBody().setTitle(catrgoryUpdate.getTitle());
        Map<String, Integer> params = new HashMap<>();
        params.put("id", id);
        ResponseEntity<BaseApiResponse> responseEntity =
                restTemplate.exchange(ROOT_URI_CATEGORY +"/"+id,HttpMethod.PUT,request,BaseApiResponse.class,params);
        return responseEntity;
    }

}
