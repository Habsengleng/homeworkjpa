package com.example.amsproject.service;

import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.ArticleUpdate;

import java.util.List;

public interface ArticleService {
    public Article save(Article article);
    public List<Article> getAll();
    public Article getOne(int id);
    public List<Article> getByCategoryTitle(String title);
    public ArticleUpdate update(ArticleUpdate article, int id);
    public Article delete(int id);
}
