package com.example.amsproject.service;

import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.ArticleUpdate;
import com.example.amsproject.repository.model.Category;
import com.example.amsproject.repository.model.CatrgoryUpdate;

import java.util.List;

public interface CategorySerivce {
    public Category insertCategory(Category category);
    public List<Category> getAll();
    public CatrgoryUpdate update(CatrgoryUpdate catrgoryUpdate, int id);
    public Category delete(int id);
}
