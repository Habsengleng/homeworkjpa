package com.example.amsproject.service.Impl;

import com.example.amsproject.repository.CategoryRepository;
import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.ArticleUpdate;
import com.example.amsproject.repository.model.Category;
import com.example.amsproject.repository.model.CatrgoryUpdate;
import com.example.amsproject.service.CategorySerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategorySerivce {
    CategoryRepository categoryRepository;
    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository){
        this.categoryRepository=categoryRepository;
    }

    @Override
    public Category insertCategory(Category category) {
        return categoryRepository.insertCategory(category);
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public CatrgoryUpdate update(CatrgoryUpdate catrgoryUpdate, int id) {
        boolean isUpdate = categoryRepository.update(catrgoryUpdate,id);
        if (isUpdate) return catrgoryUpdate;
        else return null;
    }

    @Override
    public Category delete(int id) {
        Category category = categoryRepository.getOne(id);
        boolean isDelete = categoryRepository.delete(id);
        if(isDelete) return category;
        else return null;
    }
}
