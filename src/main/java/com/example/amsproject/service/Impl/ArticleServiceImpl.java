package com.example.amsproject.service.Impl;

import com.example.amsproject.repository.AritcleRepository;
import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.ArticleUpdate;
import com.example.amsproject.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
    @PersistenceContext
    EntityManager entityManager;
    AritcleRepository aritcleRepository;
    @Autowired
    public void setAritcleRepository(AritcleRepository aritcleRepository){
        this.aritcleRepository=aritcleRepository;
    }
    @Override
    public Article save(Article article) {
        Article article1 = aritcleRepository.save(article);
        entityManager.clear();
        Article result = entityManager.find(Article.class,article1.getId());
        return result;
    }

    @Override
    public List<Article> getAll() {
        List<Article> articles = aritcleRepository.getAll();
        if(articles.isEmpty()) return null;
        return articles;
    }

    @Override
    public Article getOne(int id) {
        return aritcleRepository.getOne(id);
    }

    @Override
    public List<Article> getByCategoryTitle(String title) {
        return aritcleRepository.getByCategoryTitle(title);
    }

    @Override
    public ArticleUpdate update(ArticleUpdate article, int id) {
            boolean isUpdated = aritcleRepository.update(article, id);
            if (isUpdated) return article;
            else return null;
    }

    @Override
    public Article delete(int id) {
        Article article = aritcleRepository.getOne(id);
        boolean isDeleted = aritcleRepository.delete(id);
        if(isDeleted) return article;
        else return null;
    }
}
