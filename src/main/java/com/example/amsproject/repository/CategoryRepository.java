package com.example.amsproject.repository;

import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.Category;
import com.example.amsproject.repository.model.CatrgoryUpdate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.List;
import java.util.Queue;

@Repository
@Transactional
public class CategoryRepository {
    @PersistenceContext
    EntityManager em;

    public Category insertCategory(Category category) {
//        em.createNativeQuery("INSERT INTO tb_category (title)  VALUES (:title); ")
//                .setParameter("title", category.getTitle()).executeUpdate();
        em.persist(category);
        return category;
    }
    public List<Category> getAll(){
        CriteriaBuilder cr = em.getCriteriaBuilder();
        CriteriaQuery<Category> select = cr.createQuery(Category.class);
        Root<Category> root = select.from(Category.class);
        CriteriaQuery<Category> select2 = select.select(root);
        TypedQuery<Category> typedQuery = em.createQuery(select2);
        List<Category> resultList = typedQuery.getResultList();
        return resultList;
    }
    public boolean update(CatrgoryUpdate catrgoryUpdate, int id){
        Query query = em.createNamedQuery("update")
                .setParameter("title",catrgoryUpdate.getTitle())
                .setParameter("id",id);
        int result = query.executeUpdate();
        return result!=0;
    }
    public boolean delete(int id){
        Query query = em.createQuery("DELETE FROM Category c WHERE c.id = ?1").setParameter(1,id);
        int result = query.executeUpdate();
        return result!=0;
    }
    public Category getOne(int id){
        Query query = em.createQuery("SELECT c FROM Category c WHERE c.id = :id").setParameter("id",id);
        return (Category) query.getSingleResult();
    }
}
