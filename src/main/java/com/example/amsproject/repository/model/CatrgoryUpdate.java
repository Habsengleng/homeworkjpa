package com.example.amsproject.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CatrgoryUpdate {
    @JsonIgnore
    int id;
    private String title;

    public CatrgoryUpdate() {
    }

    public CatrgoryUpdate(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
