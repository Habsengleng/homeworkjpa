package com.example.amsproject.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity
@Table(name="tb_articles")
//@NamedQuery(query = "SELECT a FROM Article a  Join Fetch a.category c Where c.title Like lower(concat('%',:title,'%'))",name = "getByCategoryTitle")
@NamedQuery(query = "SELECT a FROM Article a  JOIN FETCH a.category c Where c.title Like '%'||:title||'%'",name = "getByCategoryTitle")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String author;
    String description;
    String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    Category category;

    public Article() {
    }

    public Article(String author, String description, String title) {
        this.author = author;
        this.description = description;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
