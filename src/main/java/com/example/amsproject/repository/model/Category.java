package com.example.amsproject.repository.model;


import javax.persistence.*;

@Entity
@Table(name = "tb_category")
@NamedQuery(query = "Update Category c SET c.title = :title WHERE c.id = :id",name = "update")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String title;

    public Category() {
    }

    public Category(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
