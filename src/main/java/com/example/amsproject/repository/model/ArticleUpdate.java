package com.example.amsproject.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ArticleUpdate {
    @JsonIgnore
    int id;
    String author;
    String description;
    String title;
    Category category;

    public ArticleUpdate() {
    }

    public ArticleUpdate(String author, String description, String title, Category category) {
        this.author = author;
        this.description = description;
        this.title = title;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
