package com.example.amsproject.repository;

import com.example.amsproject.repository.model.Article;
import com.example.amsproject.repository.model.ArticleUpdate;
import com.example.amsproject.repository.model.Category;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class AritcleRepository {
    @PersistenceContext
    EntityManager em;

    public Article save(Article article) {
        em.persist(article);
        return article;
    }
    public List<Article> getAll(){
        Query query = em.createQuery("SELECT a FROM Article a");
        return query.getResultList();
    }
    public Article getOne(int id){
        Query query = em.createQuery("SELECT a FROM Article a WHERE a.id = :id").setParameter("id",id);
        Article article = null;
        try{
            article = (Article) query.getSingleResult();
        }catch (NoResultException e){}
        return article;
    }
    public List<Article> getByCategoryTitle(String title){
        System.out.println(title);
        Query query = em.createNamedQuery("getByCategoryTitle").setParameter("title", title);
        return  query.getResultList();
    }
    public boolean update(ArticleUpdate article, int id){
        CriteriaBuilder cr = em.getCriteriaBuilder();
        CriteriaUpdate<Article> update = cr.createCriteriaUpdate(Article.class);
        Root<Article> root = update.from(Article.class);
        update.set("author",article.getAuthor());
        update.set("description",article.getDescription());
        update.set("title",article.getTitle());
        update.set("category",article.getCategory());
        update.where(cr.equal(root.get("id"),id));
        int result = em.createQuery(update).executeUpdate();
        return result != 0;
    }
    public Boolean delete(int id){
        CriteriaBuilder cr = em.getCriteriaBuilder();
        CriteriaDelete<Article> delete = cr.createCriteriaDelete(Article.class);
        Root<Article> root = delete.from(Article.class);
        delete.where(cr.equal(root.get("id"),id));
        int result = em.createQuery(delete).executeUpdate();
        return result!=0;
    }
}
