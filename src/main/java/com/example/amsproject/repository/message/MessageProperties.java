package com.example.amsproject.repository.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MessageProperties {
    private MessageSource messageSource;
    private MessageSourceAccessor accessor;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource);
    }

    public String insertSuccess(String resource) {
        return accessor.getMessage("message.inserted", new Object[]{resource});
    }

    public String updateSuccess(String resource) {
        return accessor.getMessage("message.updated", new Object[]{resource});
    }

    public String deleteSuccess(String resource) {
        return accessor.getMessage("message.deleted", new Object[]{resource});
    }

    public String selectOneSuccess(String resource) {
        return accessor.getMessage("message.selected-one", new Object[]{resource});
    }
    public String SelectedSuccess(String resource){
        return accessor.getMessage("message.selected",new Object[]{resource});
    }
}
